package org.example;

import java.util.Scanner;

public class Calculator {
    public void calculator() {
        Scanner input = new Scanner(System.in);
        char operator;

        System.out.println("Operator yang tersedia : \n - \n + \n * \n / \n % ");
        System.out.print("Masukkan Angka 1 : ");
        int value1 = input.nextInt();

        System.out.print("Operator : ");
        int i = 0;
        operator = input.next().charAt(i);

        System.out.print("Masukkan Angka 2 : ");
        int value2 = input.nextInt();

        if(operator == '+') {
            int hasil = value1 + value2;
            System.out.println("Hasilnya adalah " + hasil);
        } else if(operator == '-') {
            int hasil = value1 - value2;
            System.out.println("Hasilnya adalah " + hasil);
        } else if(operator == '*') {
            int hasil = value1 * value2;
            System.out.println("Hasilnya adalah " + hasil);
        } else if(operator == '/') {
            float hasil = value1 / value2;
            System.out.println("Hasilnya adalah " + hasil);
        } else if(operator == '%') {
            float hasil = value1 % value2;
            System.out.println("Hasilnya adalah " + hasil);
        }
    }
}
