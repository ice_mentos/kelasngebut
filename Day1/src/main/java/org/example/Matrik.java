package org.example;

import java.util.Scanner;

public class Matrik {
    public void showMatrik(){
        int matriks1[][] = new int[10][10];
        int matriks2[][] = new int[10][10];
        int hasil[][] = new int[10][10];
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan jumlah baris matriks: ");
        int m = scan.nextInt();
        System.out.print("Masukkan jumlah kolom matriks: ");
        int n = scan.nextInt();
        System.out.println("Masukkan elemen matriks pertama: ");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                matriks1[i][j] = scan.nextInt();
            }
        }
        System.out.println("Masukkan elemen matriks kedua: ");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                matriks2[i][j] = scan.nextInt();
            }
        }
        System.out.println("Hasil penjumlahan matriks: ");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                hasil[i][j] = matriks1[i][j] + matriks2[i][j];
                System.out.print(hasil[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
