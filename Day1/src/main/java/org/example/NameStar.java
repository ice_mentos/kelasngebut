package org.example;

public class NameStar {
    public void myName() {
        int [][] arrayMulti ={
                {1,1,1  ,0,0,  0,1,0, 0,0, 1,0,0, 0,0, 0,1,0, 0,0, 0,1,1},
                {1,0,0  ,0,0,  0,1,0, 0,0, 1,0,0, 0,0, 1,0,1, 0,0, 1,0,0},
                {1,1,1  ,0,0,  0,1,0, 0,0, 1,0,0, 0,0, 1,1,1, 0,0, 1,1,1},
                {1,0,1  ,0,0,  0,1,0, 0,0, 1,0,0, 0,0, 1,0,1, 0,0, 0,0,1},
                {1,1,1  ,0,0,  0,1,0, 0,0, 1,1,1, 0,0, 1,0,1, 0,0, 1,1,1}
        };
        for ( int i = 0; i < 5; i++) {
            for ( int j = 0; j < 23; j++) {
                if (arrayMulti[i][j] == 1){
                    System.out.print("*");
                }else if (arrayMulti[i][j] == 0){
                    System.out.print(" ");
                }
            }
            System.out.println("");
        }
    }
}
