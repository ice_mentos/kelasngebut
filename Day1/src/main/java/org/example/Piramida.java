package org.example;

import java.util.Scanner;

public class Piramida {
    public void myPiramida() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukan Angka : ");
        int input = scan.nextInt();

        for (int i = 1; i <= input ; i++) {
//            System.out.print("Ini Tinggi =>");
            for (int j = 1 ; j <= input-i ; j++){
                System.out.print(" ");
            }
            for (int k = 1 ; k <= i ; k++){
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
